import java.io.Serializable;

public class Carro implements Serializable{
    /**
     * Atributos
     */
    private String placa;
    private String fabricante;
    
    /***
     * Constructor
     */
    public Carro(String placa, String fabricante) {
        this.placa = placa;
        this.fabricante = fabricante;
    }

    /******************
     * Consultores
     ******************/

    public String getPlaca() {
        return placa;
    }

    public String getFabricante() {
        return fabricante;
    }

    /*****************
     * Modificadores
     ****************/

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    /************
     * Acciones
     ***********/

}