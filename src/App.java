import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/*************************************************************
 * https://gitlab.com/mision_tic_2022/utp/ciclo2_2021/p16
 **************************************************************/

public class App {
    public static void main(String[] args) throws Exception {
        //crear_carros("./carros.txt");
        leer_carros("./carros.txt");
    }

    /****
     * Método para crear objetos tipo Carro y almacenarlos en un fichero
     * @param file
     */
    public static void crear_carros(String file){
        //Creación de objetos tipo Carro
        Carro objCarro_1 = new Carro("LIU876", "Tesla");
        Carro objCarro_2 = new Carro("NHT098", "BMW");
        
        try {

            //Crear objeto tipo stream para la salida de datos
            FileOutputStream fichero = new FileOutputStream(file);
            //Crear objeto tipo stream para manejar objetos
            ObjectOutputStream objStream = new ObjectOutputStream(fichero);
            //Envío de objetos por medio de stream al fichero
            objStream.writeObject(objCarro_1);
            objStream.writeObject(objCarro_2);
            objStream.close();
        } catch (Exception e) {
            //TODO: handle exception
            System.err.println("Error");
        }        
    }

    /**
     * Método para leer objeos tipo Carro a partir de un fichero
     * @param file
     */
    public static void leer_carros(String file){

        try {
            //Referencia fichero para la entrada de datos
            FileInputStream fichero = new FileInputStream(file);
            //Crear objeto para la comunicación entre el fichero y nuestro programa
            ObjectInputStream objStream = new ObjectInputStream(fichero);
            //Obtener los objetos por medio de ObjectInputStream
            Carro objCarro_1 = (Carro) objStream.readObject();
            Carro objCarro_2 = (Carro) objStream.readObject();

            //Acceder a los objetos
            System.out.println("----------Carro 1---------");
            System.out.println("Placa: "+objCarro_1.getPlaca());
            System.out.println("Fabricante: "+objCarro_1.getFabricante());
            System.out.println("----------Carro 2---------");
            System.out.println("Placa: "+objCarro_2.getPlaca());
            System.out.println("Fabricante: "+objCarro_2.getFabricante());
            objStream.close();
        } catch (Exception e) {
            //TODO: handle exception
            System.err.println("Error al cargar los objetos");
        }

    }
}
